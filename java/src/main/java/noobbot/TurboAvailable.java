package noobbot;

/**
 * Turbo multiplies the power of your car's engine for a short period. The server indicates the availability of turbo using the turboAvailable message.
 * 
 * @author YanQin
 *
 */

class TurboAvailable
{
	final double turboDurationMilliseconds;
	final int turboDurationTicks;
	final double turboFactor;
	
	int atGameTick= 0;
	
	public int getAtGameTick() {
		return atGameTick;
	}

	public void setAtGameTick(int atGameTick) {
		this.atGameTick = atGameTick;
	}

	TurboAvailable(double turboDurationMilliseconds, int turboDurationTicks, double turboFactor)
	{
		this.turboDurationMilliseconds = turboDurationMilliseconds;
		this.turboDurationTicks = turboDurationTicks;
		this.turboFactor = turboFactor;
	}
	
	
	@Override
	public String toString()
	{
		return "DurationTicks: " + this.turboDurationTicks + ", turboFactor:" 
				+ this.turboFactor + ", DurationMillisec: " + this.turboDurationMilliseconds;
	}
}