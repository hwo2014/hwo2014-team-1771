package noobbot;

class AllCarsPosition
{
	public final Object carPositionsArr;
	
	AllCarsPosition(final Object carPositionsArr)
	{
		this.carPositionsArr = carPositionsArr;
	}
}

class CarPosition
{
	public final CarId id;
	public final double angle;
	public final PiecePosition piecePosition;
	
	CarPosition(final CarId id, final double angle, final PiecePosition piecePosition)
	{
		this.id = id;
		this.angle = angle;
		this.piecePosition = piecePosition;
	}
	
    int getPieceIndex()
    {
    	return piecePosition.pieceIndex;
    }
    
    double getInPieceDistance()
    {
    	return piecePosition.inPieceDistance;
    }
    
    int getLane()
    {
    	return piecePosition.lane.endLaneIndex;
    }
}

class CarId
{
	public final String name;
	public final String color;
	
	CarId(final String name, final String color)
	{
		this.name = name;
		this.color = color;
	}
}

class PiecePosition
{
	public final int pieceIndex;
	public final double inPieceDistance;
	public final Lane lane;
	public final int lap;
	
	PiecePosition(final int pieceIndex, final double inPieceDistance, final Lane lane, final int lap)
	{
		this.pieceIndex = pieceIndex;
		this.inPieceDistance = inPieceDistance;
		this.lane = lane;
		this.lap = lap;
	}
}

