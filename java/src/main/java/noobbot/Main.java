package noobbot;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;

import com.google.gson.Gson;

public class Main 
{
    public static void main(String... args) throws IOException 
    {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];

        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        final Socket socket = new Socket(host, port);
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

        new Main(reader, writer, botName, botKey);        
    }

    final Gson gson = new Gson();
    private PrintWriter writer;
    int gameTick;    
    
    //Writer to file
    final PrintWriter pw = new PrintWriter(new File("results.txt"));   
    
    //**************************//
    //**** Global Variables ****//
    //**************************//    
    //Variables used for max speed computation
    double throttlePercent;
    int curCarPieceIndex, prevCarPieceIndex;
    double prevDist, curDist;
    double prevPieceLength;
    ArrayList<Integer> bendPiecesIndexArr;
    int curBendPieceArrIndex;
    int startSectionIndex;
	int endSectionIndex;
    double maxCarAngle;
    CarPosition ourCarPosition = null;
    
    //Variable used for tuning latAccel when car crash
    boolean botHasCrash = false;
    boolean botHasSpawn = true;
    boolean botHasCrashCurLap = false;
    double newLatAccel;
    double totalBendSectionDist;
    double botCrashedDistOnBend;
     
    //Change in car angle var
    double totalChangeInCarAngle;
    double curChangeInCarAngle;
    double prevCarAngle;
    //double avgCarAngle;
    int carAngleTickCount;
    ArrayList<BendSection> bendSectionsArr;
    int sectionIndex;    
    
    double speed, prevSpeed;
    double logAccel;
    double totalDecel;
    int avgDecelCount;
    boolean checkInitialDecel;
    Piece[] piecesArr;
    
    //variable used for switchLane logic        
    int[] switchLanePieceIndex;
    int curSwitchIndDecision;
    Track track;
    Lane[] lanes;
    boolean firstRun;
    int lastCheckPiece;
    String decision = "";
    int innerLaneIndex;
    
    //turbo test
    boolean turboOn;
    TurboAvailable turboInfo;
    boolean turboTrigger;
    
    int lapCount;
    int raceCount = 0;
    
    final boolean TURBO_MODE = true;
    final boolean CI_BUILD = true;
    final boolean DEFAULT_BOT = false;
    final boolean WRITE_TO_FILE = false;
    
    //***************************//
    //**** TUNING PARAMETERS ****//
    //***************************//
    final double ACCEL_DECEL_RATE = 0.2;
    final double INITIAL_LAT_ACCEL = 0.39;
    final double INITIAL_DECEL = -0.15;
    final double LAT_ACCEL_ADJUST = 0.005;
    final int TURBO_ACTIVATION_DIST = 300;
    final int BRAKING_DIST_OFFSET = 0; //Original value = 15
    final int CAR_ANGLE_LIMIT_STOP_ADJUST = 50;
    final int CAR_ANGLE_LIMIT_ADJUST_LOWER = 58;
    
    public Main(final BufferedReader reader, final PrintWriter writer, String botName, String botKey) throws IOException 
    {
        this.writer = writer;
        String line = null;
        String botColor = "";
        
        BendSection curBendSection;
        
        if(CI_BUILD)
        {
        	//Join quick race
            Join join = new Join(botName, botKey);
            send(join);
        }
        else
        {
        	//Create race and join
            CreateRace createRace = new CreateRace(botName, botKey, "pentag", "", 1);
            //JoinRace createRace = new JoinRace(botName, botKey, "imola", "password$1", 2);
            send(createRace);
        }

        while((line = reader.readLine()) != null) 
        {
        	//Get message from server
            final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);

            gameTick = msgFromServer.gameTick;
            
            if (msgFromServer.msgType.equals("carPositions")) 
            {
            	//System.out.println("Server send data: "+gson.toJson(msgFromServer.data));
            	//writeToFile("Server send data: "+gson.toJson(msgFromServer.data));
            	
            	CarPosition[] carsPositionArr = gson.fromJson(gson.toJson(msgFromServer.data), CarPosition[].class);      
            	
            	for(int i=0; i<carsPositionArr.length; i++)
            	{
            		if(carsPositionArr[i].id.name.equals(botName) && carsPositionArr[i].id.color.equals(botColor))
            		{
            			ourCarPosition = carsPositionArr[i];
            			
            			break;
            		}
            	}

            	//Get current car location on piece
            	PiecePosition curCarPiecePos = ourCarPosition.piecePosition;  
            	curCarPieceIndex = curCarPiecePos.pieceIndex; 
            	Piece curPiece = piecesArr[curCarPieceIndex];
            	
            	//Get current bend section details
            	curBendSection = bendSectionsArr.get(sectionIndex);    			
    			startSectionIndex = curBendSection.getStartIndexOfSection();
				endSectionIndex = curBendSection.getEndIndexOfSection();
            	
            	//*************************//
            	//***** Compute Speed *****//
            	//*************************//
            	curDist = curCarPiecePos.inPieceDistance;      
            	
            	//Check if bot crash and tune down latAccel
            	if(botHasCrash)
            	{
            		double ratioOfCircumference;
    				double circumference;
    				
            		botHasCrash = false;
            		
            		System.out.println("Bot has crash at piece "+curCarPieceIndex+" at latAccel of "+curPiece.getLatAccel());
            		writeToFile("Bot has crash at piece "+curCarPieceIndex+" at latAccel of "+curPiece.getLatAccel());            		
            		
            		if(curPiece.isBend())
            		{
            			//Compute the total distance of the bend section            			
            			totalBendSectionDist = 0;
            			botCrashedDistOnBend = 0;
            			
            			for(int i=startSectionIndex; i<(endSectionIndex+1); i++)
            			{
            				ratioOfCircumference = Math.abs(piecesArr[i].angle/360);
            				circumference = (2*Math.PI*(getRadius(piecesArr[i], lanes, ourCarPosition)));
            				
            				totalBendSectionDist += ratioOfCircumference*circumference;
            				            				
            				//Compute bot crashed distance
            				if(i == ourCarPosition.piecePosition.pieceIndex)
            					//Bot crash on this piece
            					botCrashedDistOnBend += ourCarPosition.piecePosition.inPieceDistance;            				
            				else if(i < ourCarPosition.piecePosition.pieceIndex)
            					botCrashedDistOnBend += ratioOfCircumference*circumference;
            			}
            			writeToFile("Total Bend Distance: "+totalBendSectionDist+" Bot crash at distance: "+botCrashedDistOnBend);
            			       
            			//Tune down the latAccel for all the bend in the section where the bot crashes
            			for(int i=startSectionIndex; i<(endSectionIndex+1); i++)           
    					{          
            				piecesArr[i].crashCount++;
            				
    						newLatAccel = piecesArr[i].getLatAccel()-(LAT_ACCEL_ADJUST*piecesArr[i].crashCount*(totalBendSectionDist/botCrashedDistOnBend));
    						piecesArr[i].setLatAccel(newLatAccel); 
    						
    						System.out.println("Tune down LatAccel for Piece "+i+" to "+newLatAccel+" crashCount: "+piecesArr[i].crashCount);
    	            		writeToFile("Tune down LatAccel for Piece "+i+" to "+newLatAccel+" crashCount: "+piecesArr[i].crashCount);
    					}
            			
            			//Search for all the pieces which has a sharper turn than the current bend and set the new latAccel
            			writeToFile("Searching for similar pieces with sharper turning radius and tune latAccel");
            			for(int i=0; i<piecesArr.length; i++)
            			{
            				if(piecesArr[i].radius <= curPiece.radius && 
            						piecesArr[i].angle >= curPiece.angle)
            				{
            					if(piecesArr[i].getLatAccel() >= piecesArr[curCarPieceIndex].getLatAccel())
            					{
            						piecesArr[i].setLatAccel(piecesArr[curCarPieceIndex].getLatAccel());
            						
            						writeToFile("Tune down LatAccel for Piece "+i+" to "+newLatAccel+" crashCount: "+piecesArr[i].crashCount);
            					}
            				}
            			}
            		}
            		else //If bot crashes on a straight road, reduce the latAccel of the prev bend section
            		{
            			BendSection prevBendSection;
            			int prevStartSectionIndex;
            			int prevEndSectionIndex;
            			
            			if(sectionIndex - 1 < 0)
            				prevBendSection = bendSectionsArr.get(bendSectionsArr.size() - 1);
            			else
            				prevBendSection = bendSectionsArr.get(sectionIndex - 1);
            			
            			prevStartSectionIndex = prevBendSection.getStartIndexOfSection();
            			prevEndSectionIndex = prevBendSection.getEndIndexOfSection();
            			            			
            			//Find out the dist of the bend section that the bot crashes at
            			for(int i=prevStartSectionIndex; i<(prevEndSectionIndex+1); i++)
            			{
            				ratioOfCircumference = Math.abs(piecesArr[i].angle/360);
            				circumference = (2*Math.PI*(getRadius(piecesArr[i], lanes, ourCarPosition)));
            				
            				totalBendSectionDist += ratioOfCircumference*circumference;            				
            			}
            			writeToFile("Total Bend Distance: "+totalBendSectionDist);
            			
            			//Tune down all bend pieces in the bend section
            			for(int i=prevStartSectionIndex; i<= prevEndSectionIndex; i++)
            			{    		
            				piecesArr[i].crashCount++;
            				
    						newLatAccel = piecesArr[i].getLatAccel()-(LAT_ACCEL_ADJUST*piecesArr[i].crashCount*(totalBendSectionDist/(totalBendSectionDist+curCarPiecePos.inPieceDistance)));
    						piecesArr[i].setLatAccel(newLatAccel);
    						piecesArr[i].crashOnStraightRoad = true;
    						
    						System.out.println("Tune down LatAccel for Piece "+i+" to "+newLatAccel+" crashCount: "+piecesArr[i].crashCount);
    	            		writeToFile("Tune down LatAccel for Piece "+i+" to "+newLatAccel+" crashCount: "+piecesArr[i].crashCount);
            			}
            			
            			//Search for all the pieces which has a sharper turn than the current bend and set the new latAccel
            			writeToFile("Searching for similar pieces with sharper turning radius and tune latAccel");
            			for(int i=0; i<piecesArr.length; i++)
            			{
            				if(piecesArr[i].radius <= piecesArr[prevEndSectionIndex].radius && 
            						piecesArr[i].angle >= piecesArr[prevEndSectionIndex].angle)
            				{
            					if(piecesArr[i].getLatAccel() >= piecesArr[prevEndSectionIndex].getLatAccel())
            					{
            						piecesArr[i].setLatAccel(piecesArr[prevEndSectionIndex].getLatAccel());
            						
            						writeToFile("Tune down LatAccel for Piece "+i+" to "+newLatAccel+" crashCount: "+piecesArr[i].crashCount);
            					}
            				}
            			}
            		}
            	}
            	
            	//Car moved to next piece
            	if(curCarPieceIndex != prevCarPieceIndex)
            	{
            		speed = (prevPieceLength + curDist) - prevDist;
            		
            		//Adjust max speed of bend            		
            		if(piecesArr[prevCarPieceIndex].isBend())
            		{
            			//Check if last bend is the end of a section
            			if(prevCarPieceIndex == endSectionIndex)
            			{
            				System.out.println("Max angle of last section from index "+startSectionIndex+" to "+endSectionIndex+" is "+maxCarAngle);
            				writeToFile("Max angle of last section from index "+startSectionIndex+" to "+endSectionIndex+" is "+maxCarAngle);
            				            				
            				//Tune the latAccel for the bend on the last section
            				if(botHasCrashCurLap == false && piecesArr[prevCarPieceIndex].crashOnStraightRoad == false && 
            						maxCarAngle < CAR_ANGLE_LIMIT_STOP_ADJUST && lapCount > 0)
            				{
            					for(int i=startSectionIndex; i<(endSectionIndex+1); i++)           
            					{
            						if(maxCarAngle == 0)
            							//Avoid division by zero error
            							newLatAccel = piecesArr[i].getLatAccel()+(LAT_ACCEL_ADJUST*(CAR_ANGLE_LIMIT_STOP_ADJUST/0.1));
            						else
            							newLatAccel = piecesArr[i].getLatAccel()+(LAT_ACCEL_ADJUST*(CAR_ANGLE_LIMIT_STOP_ADJUST/maxCarAngle));
            						
            						piecesArr[i].setLatAccel(newLatAccel); 
            						
            						System.out.println("Tune up LatAccel for Piece "+i+" to "+newLatAccel);
            	            		writeToFile("Tune up for Piece "+i+" to "+newLatAccel);
            					}
            				}
            				//Tune down the latAccel for the bend if max angle is 58 and above
            				else if(maxCarAngle >= CAR_ANGLE_LIMIT_ADJUST_LOWER)
            				{
            					for(int i=startSectionIndex; i<(endSectionIndex+1); i++)           
            					{
            						newLatAccel = piecesArr[i].getLatAccel()-LAT_ACCEL_ADJUST;
            						piecesArr[i].setLatAccel(newLatAccel); 
            						
            						System.out.println("Tune down LatAccel for Piece "+i+" to "+newLatAccel);
            	            		writeToFile("Tune down LatAccel for Piece "+i+" to "+newLatAccel);
            					}
            				}
            				
            				maxCarAngle = 0;            				
            				
            				if(sectionIndex == bendSectionsArr.size() - 1)
            					sectionIndex = 0;
            				else
            					sectionIndex++;
            				
            				//Refresh the correct start section index so below codes can reflect correct values
            				curBendSection = bendSectionsArr.get(sectionIndex);    			
                			startSectionIndex = curBendSection.getStartIndexOfSection();
            				endSectionIndex = curBendSection.getEndIndexOfSection();
            			}
            		}
            	}
            	else
            	{
            		speed = curDist - prevDist;
            		            		
            		if(curPiece.isBend())
            		{
            			//Max car angle heuristic for adjusting latAccel
            			if(maxCarAngle < Math.abs(ourCarPosition.angle))
            				maxCarAngle = Math.abs(ourCarPosition.angle);
            		}
            	}
            	
            	//Log the acceleration and deceleration
            	logAccel = speed - prevSpeed;
            	
            	prevSpeed = speed;
            	
            	prevCarPieceIndex = curCarPieceIndex;
            	prevDist = curDist;
            	
            	//Check if road is bend or straight to compute distance
            	if(curPiece.length == 0)
            		prevPieceLength = (Math.abs(curPiece.angle)/360)*(2*Math.PI*(getRadius(curPiece, lanes, ourCarPosition)));
            	else
            		prevPieceLength = curPiece.length;
            	
            	//***************************//
            	//**** Throttle Formulas ****//
            	//***************************//
            	            	
            	//Formula       	
            	//Lateral Acceleration = (V*V)/R
                    	
            	//Max speed formula
            	int nextCarPieceIndex, nextBendIndex;
            	double maxSpeedOnNextPiece, maxSpeedOnCurPiece;
            	
            	//Get index of upcoming piece
            	if((curCarPieceIndex + 1) > (piecesArr.length - 1))
            		nextCarPieceIndex = 0;
            	else
            		nextCarPieceIndex = curCarPieceIndex + 1;
            	
            	//Check if upcoming piece is a bend
            	nextBendIndex = bendPiecesIndexArr.get(curBendPieceArrIndex);
            	
            	//Check if car reaches the bend, if reached, increment to check on next bend
            	if(curCarPieceIndex == nextBendIndex)
            	{
        			curBendPieceArrIndex++;
            		
            		//End of lap, reset bend array index
            		if(curBendPieceArrIndex == bendPiecesIndexArr.size())
            			curBendPieceArrIndex = 0;
            		
            		nextBendIndex = bendPiecesIndexArr.get(curBendPieceArrIndex); 
            	}
            	
            	if(nextCarPieceIndex == nextBendIndex)            	
            		maxSpeedOnNextPiece = piecesArr[nextBendIndex].getMaxSpeed();
            	else
            		maxSpeedOnNextPiece = speed;
            	
            	maxSpeedOnCurPiece = piecesArr[curCarPiecePos.pieceIndex].getMaxSpeed();
            	
            	//**********************************//
            	//**** Compute Braking Distance ****//
            	//**********************************//
            	double curBrakeSpeed = speed;
            	double curBrakeDist = 0;
            	
            	//Calculate the braking distance needed to the required speed
            	//START --- New braking distance code
            	double maxBrakingSpeedLimit = 0;
            	
            	//Get the limit max speed of the next bend 
            	if(curPiece.isBend() == false)            	
            		maxBrakingSpeedLimit = piecesArr[startSectionIndex].getMaxSpeed();            	
            	else
            	{
            		BendSection nextBendSection;
            		
            		if(sectionIndex+1 == bendSectionsArr.size())            			
            			nextBendSection = bendSectionsArr.get(0);
            		else
            			nextBendSection = bendSectionsArr.get(sectionIndex+1);
            		
            		maxBrakingSpeedLimit = piecesArr[nextBendSection.getStartIndexOfSection()].getMaxSpeed();
            	}
            	
            	while(curBrakeSpeed > maxBrakingSpeedLimit)
            	{
            		curBrakeDist += curBrakeSpeed;
            		
            		//Remove the initial decel value to compute more accurate decel rate
            		if(avgDecelCount == 2 && checkInitialDecel == false)
            		{
            			checkInitialDecel = true;
            			
            			totalDecel = totalDecel - INITIAL_DECEL;
            			avgDecelCount--;
            		}
            			
            		curBrakeSpeed = curBrakeSpeed + (totalDecel/avgDecelCount);                		                		
            	}
            	//END --- New braking distance code
            	
            	
            	//Calculate distance to next bend
            	double distToNextBend = 0;
            	            	
            	//START --- (New) Compute distance to next bend
            	int nextStartSectionIndex = 0;
            	
            	if(curPiece.isBend() == false)
            	{
            		//Compute distance travelled on current piece
            		distToNextBend = curPiece.length - curCarPiecePos.inPieceDistance;
            		
            		nextStartSectionIndex = startSectionIndex;
            	}
            	else
            	{
            		//Compute distance travelled on current bend
            		BendSection nextBendSection;
            		
            		if(sectionIndex+1 == bendSectionsArr.size())            			
            			nextBendSection = bendSectionsArr.get(0);
            		else
            			nextBendSection = bendSectionsArr.get(sectionIndex+1);
            		
            		nextStartSectionIndex = nextBendSection.getStartIndexOfSection();
            		
            		distToNextBend = (Math.abs(curPiece.angle/360)*(2*Math.PI*getRadius(curPiece, lanes, ourCarPosition))) - curCarPiecePos.inPieceDistance;            		
            	}
            	
        		if(curCarPieceIndex > nextStartSectionIndex)
            	{
        			for(int i=curCarPieceIndex+1; i<piecesArr.length; i++)
        			{
        				if(piecesArr[i].isBend())
        					distToNextBend += (Math.abs(piecesArr[i].angle/360)*(2*Math.PI*getRadius(piecesArr[i], lanes, ourCarPosition)));
        				else
        					distToNextBend += piecesArr[i].length;
        			}                			
        			
        			for(int i=0; i<nextStartSectionIndex; i++)
        			{
        				if(piecesArr[i].isBend())
        					distToNextBend += (Math.abs(piecesArr[i].angle/360)*(2*Math.PI*getRadius(piecesArr[i], lanes, ourCarPosition)));
        				else
        					distToNextBend += piecesArr[i].length;
        			}        			
            	}
            	else
            	{
            		for(int i=curCarPieceIndex+1; i<nextStartSectionIndex; i++)
            		{
            			if(piecesArr[i].isBend())
        					distToNextBend += (Math.abs(piecesArr[i].angle/360)*(2*Math.PI*getRadius(piecesArr[i], lanes, ourCarPosition)));
        				else
        					distToNextBend += piecesArr[i].length;
            		}
            	}
            	//END --- (New) Compute distance to next bend
            	
            	//Brake based on braking distance on straight road approaching a bend
            	if(DEFAULT_BOT == true)
            	{
            		throttlePercent = 0.5;
            	}
            	else if(curPiece.isBend() == false)
            	{
            		double braking_offset = 0;
            		
            		//Hardcode braking dist offset to test out performance
            		braking_offset = BRAKING_DIST_OFFSET*(piecesArr[nextBendIndex].radius/50);
            		
            		//if(curBrakeDist >= (distToNextBend+BRAKING_DIST_OFFSET))            				
            		if(curBrakeDist >= (distToNextBend+braking_offset))
        				throttlePercent = 0;
            		else if(Math.abs(ourCarPosition.angle) >= CAR_ANGLE_LIMIT_STOP_ADJUST)
            		{
            			/*
            			if(throttlePercent - ACCEL_DECEL_RATE < 0)
                			throttlePercent = 0;
                		else
                			throttlePercent -= ACCEL_DECEL_RATE;
                		*/
            			
            			throttlePercent = 0;
            		}
            		else
            		{
            			if(throttlePercent + ACCEL_DECEL_RATE > 1)
                			throttlePercent = 1;
                		else
                			throttlePercent += ACCEL_DECEL_RATE;
            		}
            	}
            	//In a bend
            	else
            	{
            		if(curBrakeDist >= distToNextBend)            		
            			throttlePercent = 0;   
            		//Check that car has reach the end of the bend of a section and avoid over-steering
            		else if(curCarPieceIndex == endSectionIndex && Math.abs(ourCarPosition.angle) >= CAR_ANGLE_LIMIT_STOP_ADJUST)            			
            			throttlePercent = 0; 
            		else if(speed > maxSpeedOnNextPiece || Math.abs(ourCarPosition.angle) >= CAR_ANGLE_LIMIT_ADJUST_LOWER)
            		//else if(speed > maxSpeedOnCurPiece || Math.abs(ourCarPosition.angle) >= CAR_ANGLE_LIMIT_ADJUST_LOWER)
            		{
            			if(throttlePercent - ACCEL_DECEL_RATE < 0)
                			throttlePercent = 0;
                		else
                			throttlePercent -= ACCEL_DECEL_RATE;
                	}
                	else
                	{
                		if(throttlePercent + ACCEL_DECEL_RATE > 1)
                			throttlePercent = 1;
                		else
                			throttlePercent += ACCEL_DECEL_RATE;
                	}
            	}
            	
            	//Log total deceleration
            	if(botHasSpawn && throttlePercent == 0 && logAccel < 0)
            	{
            		avgDecelCount++;
            		totalDecel += logAccel;
            	}
            		
            	
            	//Log results to file
            	String strLogResult = "GameTick: "+gameTick+"\tLap: "+lapCount+"\tInPieceDist: "+curCarPiecePos.inPieceDistance+"\tSpeed: "+speed+"/tick\tAccel/Decel: "+logAccel+"\tPieceIndex: "+curCarPieceIndex+"\tthrottle: "+throttlePercent+"\trad: "+curPiece.radius+"\tcarAngle: "+ourCarPosition.angle+"\tmaxSpeedOnNextPiece: "+maxSpeedOnNextPiece+"\tmaxSpeedOnCurrentPiece: "+maxSpeedOnCurPiece+"\tCarOnLane: "+ourCarPosition.piecePosition.lane.endLaneIndex+"\tBraking Dist: "+curBrakeDist+"\tDistToNextBend: "+distToNextBend;
            	System.out.println(strLogResult);
            	
            	//Write to file
            	writeToFile(strLogResult);
            	
            	//*****************************//
            	//**** Bot has crash logic ****//
            	//*****************************//
            	if(!botHasSpawn)
            	{
            		System.out.println("Sending ping when bot crash and has not spawn at tick "+gameTick);
            		send(new Ping(gameTick));
            		
            		//botHasSpawn = true;
            	}
            	else
            	{
            		//*****************************//
                	//***** Switch Lane Logic *****//
                	//*****************************//
                	//check for switch lane
            		HashMap<String, ArrayList<CarPosition>> carsInfront = new HashMap<String, ArrayList<CarPosition>>();
            		for(int i=0; i<carsPositionArr.length; i++)
            		{
            			CarPosition car = carsPositionArr[i];
            			if(!carsPositionArr[i].id.name.equals(botName) && !carsPositionArr[i].id.color.equals(botColor))
            			{
	            			//if car ahead of our car
	            			int carGap = car.getPieceIndex() - ourCarPosition.getPieceIndex();
	            			if((carGap == 0 && car.getInPieceDistance() > ourCarPosition.getInPieceDistance() ) || (carGap == 1)) // && carGap < 2
	            			{
	            				//System.out.println("car exist in same piece @ gameTick " + gameTick + " ourCar lane:" + ourCarPosition.getLane() + " and carLane:" + car.getLane());
	            				
	            				int laneDiff = ourCarPosition.getLane() - car.getLane();
	            				if(laneDiff == 0 )
	            				{	
	            					System.out.println("same lane");
	            					ArrayList<CarPosition> carList;
	            					if(carsInfront.get("same") == null)
	            						carList = new ArrayList<CarPosition>();
	            					else
	            						carList = carsInfront.get("same");
	            					
	            					carList.add(car);
	            					carsInfront.put("same", carList);
	            					
	            					System.out.println("size of car in SAME: " + carsInfront.get("same").size());
	            				}
	            				else if(laneDiff == -1)
	            				{
	            					ArrayList<CarPosition> carList;
	            					if(carsInfront.get("right") == null)
	            						carList = new ArrayList<CarPosition>();
	            					else
	            						carList = carsInfront.get("right");
	            					
	            					carList.add(car);
	            					carsInfront.put("right", carList);
	            				}
	              				else if(laneDiff == 1)
	            				{
	            					ArrayList<CarPosition> carList;
	            					if(carsInfront.get("left") == null)
	            						carList = new ArrayList<CarPosition>();
	            					else
	            						carList = carsInfront.get("left");
	            					
	            					carList.add(car);
	            					carsInfront.put("left", carList);
	            				}
	            			}
            			}
            		}
            		
                	if(curCarPieceIndex==0 && firstRun)
                	{
                		firstRun = false;
                		decision = toSwitchLane(switchLanePieceIndex[curSwitchIndDecision]+1, switchLanePieceIndex[curSwitchIndDecision+1]-1, track);
                		 
                		send(new Throttle(throttlePercent, gameTick));
                		writeToFile("Send throttle of "+throttlePercent+" at tick "+gameTick);
/*                		if(!decision.isEmpty())
                		{
                			send(new SwitchLane(decision, gameTick));
                		}*/
                	}
                	else if(switchLanePieceIndex[curSwitchIndDecision]+1 == curCarPieceIndex && lastCheckPiece != curCarPieceIndex)
                	{
                		lastCheckPiece = curCarPieceIndex;
                		curSwitchIndDecision++; 
                		//String decision;
                		//System.out.println("switchLanePieceIndex: " + switchLanePieceIndex.length);
                		if(curSwitchIndDecision+1 < switchLanePieceIndex.length)
                			decision = toSwitchLane(switchLanePieceIndex[curSwitchIndDecision]+1, switchLanePieceIndex[curSwitchIndDecision+1]-1, track);
                		else
                		{
                			decision = toSwitchLane(switchLanePieceIndex[curSwitchIndDecision]-1, track.pieces.length-1, track);
                			curSwitchIndDecision = 0;
                			firstRun = true;
                		}
                		
                		send(new Throttle(throttlePercent, gameTick));
                		writeToFile("Send throttle of "+throttlePercent+" at tick "+gameTick);
                		/*
                		if(!decision.isEmpty())
                		{
                			send(new SwitchLane(decision, gameTick));
                		}*/
                	}
                	//check for overtaking
                	else if(curCarPieceIndex == switchLanePieceIndex[curSwitchIndDecision]-1 && lastCheckPiece != curCarPieceIndex)
                	{
                		System.out.println("=== ENTER OVERTAKE LOGIC === Decision: " + decision + " ourCar Lane: " + ourCarPosition.getLane() +
                							", gameTick: " +  gameTick);
                		writeToFile("=== ENTER OVERTAKE LOGIC === Decision: " + decision + " ourCar Lane: " + ourCarPosition.getLane() +
                							", gameTick: " +  gameTick);
                		lastCheckPiece = curCarPieceIndex;
                		if(decision.isEmpty())
                		{
                			if(carsInfront.containsKey("same")){
                				writeToFile("SAME lane");
	                			if(ourCarPosition.getLane() == 0){
	                				writeToFile("SAME Lane: move Right");
	                				decision = "Right";
	                			}
	                			else
	                			{
	                				writeToFile("SAME Lane: move left");
	                				decision = "Left";
	                			}
                			}
                		}
                		else if( (decision.equals("Right") && ourCarPosition.getLane() == innerLaneIndex)) 
                		{       
                			writeToFile("Decision left and getLane == innerLaneIndex");
                			if(carsInfront.containsKey("same")){
                				System.out.println("Right- Car in same lane! size of carList is " + carsInfront.get("same").size());
                				writeToFile("Right- Car in same lane! size of carList is " + carsInfront.get("same").size());
                				ArrayList<CarPosition> carList = carsInfront.get("same");
                				if(carList.size() > 0 )
                				{
                					decision = "Left";		 		
                				}
                			}             
                		}
                		else if((decision.equals("Left") && ourCarPosition.getLane() == 0)) 
                		{          
                			writeToFile("Decision left and getLane == 0");
                			if(carsInfront.containsKey("same")){
                				System.out.println("LEFT- Car in same lane! size of carList is " + carsInfront.get("same").size() );
                				writeToFile("LEFT- Car in same lane! size of carList is " + carsInfront.get("same").size());
                				ArrayList<CarPosition> carList = carsInfront.get("same");
                				if(carList.size() > 0 )
                				{
                					decision = "Right";		 		
                				}
                			}             
                		}
                		else if(decision.equals("Left"))
                		{
                			writeToFile("Decision left");
                			if(!carsInfront.containsKey("same")  && carsInfront.containsKey("left"))
                				if(carsInfront.get("left").size() > 0){
                					writeToFile("cars on left");
                					decision = "";
                				}
                			
                		}
                		
                		else if(decision.equals("Right"))
                		{
                			writeToFile("Decision left");
                			if(!carsInfront.containsKey("same") && carsInfront.containsKey("right"))
                				if(carsInfront.get("right").size() > 0){
                					writeToFile("cars on right");
                					decision = "";
                				}
                		}
                		
                		if(!decision.isEmpty())
                		{
                			send(new SwitchLane(decision, gameTick));
                			writeToFile("Send switch lane at tick "+gameTick);
                		}
                		else
                		{
                			send(new Throttle(throttlePercent, gameTick));
                			writeToFile("Send throttle of "+throttlePercent+" at tick "+gameTick);
                		}
                	}
                	else
                	{
                    	//***********************//
                    	//***** Turbo Logic *****//
                    	//***********************//
                		if(turboOn && TURBO_MODE)
                		{
                			// decceleration @ -0.338 (with turbo)
                			// decceleration @ -0.025 (without turbo)

                			if(curPiece.isBend() == false && distToNextBend >= TURBO_ACTIVATION_DIST && !turboTrigger &&
                					Math.abs(ourCarPosition.angle) < CAR_ANGLE_LIMIT_ADJUST_LOWER)
                			{ //&& !curPiece.isBend()){
                				System.out.println(botName+" has turbo triggered at game tick "+gameTick);
                        		writeToFile(botName+" has turbo triggered at game tick "+gameTick);
                				send(new Turbo("Vroom vroom vrooom~", gameTick));
                				turboTrigger = true;
                			}
                			else
                			{
                				send(new Throttle(throttlePercent, gameTick));
                				writeToFile("Send throttle of "+throttlePercent+" at tick "+gameTick);
                				//send(new Throttle(0.5, gameTick));
                			}
                		}
                		else
                		{
    		            	//Send new throttle value
    		            	send(new Throttle(throttlePercent, gameTick));
    		            	writeToFile("Send throttle of "+throttlePercent+" at tick "+gameTick);
                		}
                	}
            	}
            } 
            else if (msgFromServer.msgType.equals("yourCar"))
            {
            	System.out.println("Your Car details: "+gson.toJson(msgFromServer.data));
            	writeToFile("Your Car details: "+gson.toJson(msgFromServer.data));
            	
            	CarId yourCar = gson.fromJson(gson.toJson(msgFromServer.data), CarId.class);
            	
            	if(yourCar.name.equals(botName))
            	{
            		botColor = yourCar.color;
            	}
            }
            else if (msgFromServer.msgType.equals("join")) 
            {
                System.out.println("Joined");
                writeToFile("Joined");
            }
            else if (msgFromServer.msgType.equals("gameInit")) 
            {
                System.out.println("Race init");
                writeToFile("Race init");
                
                //Init variables
                initVariables();
                
                //replace switch with laneSwitch
            	//String data = msgFromServer.data.toString();
            	//data = data.replaceAll("switch", "switchLane");
            	
            	//Initialize game and track details
            	GameInit initData = gson.fromJson(gson.toJson(msgFromServer.data).replaceAll("switch", "switchLane"), GameInit.class);
                
                System.out.println(initData.race.track.toString());
                writeToFile(initData.race.track.toString());
                
                //Only initialize the track details once during qualifying round to retain the old latAccel
                if(raceCount == 0)
                {
                	Race raceData = initData.race;
                    track = raceData.track;
                    lanes = track.lanes;
                    innerLaneIndex = track.getInnerRingIndex();
                    
                    switchLanePieceIndex = track.getSwitchLaneIndex();                
                    curSwitchIndDecision = 0;
                    
                    //Add all the bend pieces into an array
                    bendPiecesIndexArr = track.getBendIndex();
                    System.out.println("Bend pieces index: "+bendPiecesIndexArr.toString());
                    writeToFile("Bend pieces index: "+bendPiecesIndexArr.toString());                
                    
                    piecesArr = track.pieces;
                    Piece curPiece;
                    
                    //Compute each pieces max speed
                    for(int i=0; i<piecesArr.length; i++)
                    {
                    	//Initialize the max speed for all the bends
                    	piecesArr[i].computeMaxSpeed(INITIAL_LAT_ACCEL);
                    	
                    	curPiece = piecesArr[i];                    	
                    }
                    
                    //Group bend pieces together into sections to aid in the learning of latAccel                
                    int startSectionIndex = -1;
                    int curConsecIndex;
                    int prevConsecIndex = -1;
                    boolean endOfSection = false;
                    
                    for(int i=0; i < bendPiecesIndexArr.size(); i++)
                    {
                    	curConsecIndex = bendPiecesIndexArr.get(i);
                    	
                    	if(startSectionIndex == -1)
                    		startSectionIndex = curConsecIndex;
                    	                	
                    	double curConsecAngle;
                    	double prevConsecAngle;
                    	
                    	if(prevConsecIndex != -1)
                    	{
                    		curConsecAngle = piecesArr[curConsecIndex].angle/Math.abs(piecesArr[curConsecIndex].angle);
                        	prevConsecAngle = piecesArr[prevConsecIndex].angle/Math.abs(piecesArr[prevConsecIndex].angle);
                        	
    	                	if(curConsecIndex == 0)
    	                	{
    		                		if((piecesArr.length - 1) != prevConsecIndex || (curConsecAngle != prevConsecAngle))                    	
    		                			endOfSection = true;    
    	                	}
    	                	else
    	                	{
    	                			if((curConsecIndex - 1) != prevConsecIndex || (curConsecAngle != prevConsecAngle))
    	                				endOfSection = true;
    	                	}
                    	}
                    	
                    	//Check if bend reaches the end of section
                    	if(endOfSection)
                    	{
                    		bendSectionsArr.add(new BendSection(startSectionIndex, prevConsecIndex));
                    		startSectionIndex = curConsecIndex;
                    		endOfSection = false;
                    	}
                    	
                    	//Add the last bend pieces as a section
                    	if(i == bendPiecesIndexArr.size() - 1)                	
                    		bendSectionsArr.add(new BendSection(startSectionIndex, curConsecIndex));                	
                    	
                    	prevConsecIndex = curConsecIndex;
                    }
                    
                    for(int i=0; i<bendSectionsArr.size(); i++)
                    	writeToFile(bendSectionsArr.get(i).toString());
                }
                
                //Print out track details
                for(int i=0; i<piecesArr.length; i++)
                {
                	Piece curPiece = piecesArr[i];
                	
                	System.out.println("Index: "+i+" Length: "+curPiece.length+" Radius: "+curPiece.radius+" Angle: "+curPiece.angle+" Switch: "+curPiece.switchLane+" LatAccel: "+curPiece.getLatAccel());
                	writeToFile("Index: "+i+" Length: "+curPiece.length+" Radius: "+curPiece.radius+" Angle: "+curPiece.angle+" Switch: "+curPiece.switchLane+" LatAccel: "+curPiece.getLatAccel());
                }
            } 
            else if (msgFromServer.msgType.equals("turboAvailable")) 
            {
            	//send(new Turbo("Vroom", gameTick));
            	
            	if(botHasSpawn)
            	{
            		turboInfo = gson.fromJson(gson.toJson(msgFromServer.data), TurboAvailable.class);
                	turboInfo.setAtGameTick(gameTick);
                	turboOn = true;
                	turboTrigger = false;
                	
                	System.out.println("Turbo available at game tick "+gameTick+" (Duration: "+turboInfo.turboDurationMilliseconds+") (Duration in ticks: "+turboInfo.turboDurationTicks+") (Turbo factor: "+turboInfo.turboFactor+")");
            		writeToFile("Turbo available at game tick "+gameTick+" duration: "+turboInfo.turboDurationMilliseconds+" duration in ticks: "+turboInfo.turboDurationTicks+" turbo factor: "+turboInfo.turboFactor);
            	}
            } 
/*            else if (msgFromServer.msgType.equals("turboStart")) 
            {            	
            	initSpeed = speed;
            } */
            else if (msgFromServer.msgType.equals("turboEnd")) 
            {      
            	
            	CarId carId = gson.fromJson(gson.toJson(msgFromServer.data), CarId.class);
            	
            	if(carId.name.equals(botName) && carId.color.equals(botColor))
            	{
            		System.out.println(botName+" end turbo at "+gameTick);
            		writeToFile(botName+" end turbo at "+gameTick);
            		
            		turboOn = false;
            	}
            }
            else if (msgFromServer.msgType.equals("gameEnd")) 
            {
                System.out.println("Race end");
                System.out.println("Avg Decel: "+totalDecel/avgDecelCount+" Count: "+avgDecelCount);
                writeToFile("Race end");
                
                raceCount++;
            }
            else if (msgFromServer.msgType.equals("gameStart")) 
            {
                System.out.println("Race start, GameTick: "+gameTick);
                writeToFile("Race start");
                
                send(new Throttle(1.0, gameTick));
            }
            else if(msgFromServer.msgType.equals("lapFinished"))
            {
            	LapFinished lapFinished = gson.fromJson(gson.toJson(msgFromServer.data), LapFinished.class);
            	CarId ourCar = lapFinished.car;
            	
            	System.out.println(gson.toJson(msgFromServer.data));
            	writeToFile(gson.toJson(msgFromServer.data));
            	
            	if(ourCar.name.equals(botName) && ourCar.color.equals(botColor))
            	{
            		lapCount++;
                	botHasCrashCurLap = false;
            	}
            }
            else if(msgFromServer.msgType.equals("tournamentEnd"))
            {
            	System.out.println("Tournament End");
                writeToFile("Tournament End");                
                pw.close();
            }
            else if(msgFromServer.msgType.equals("crash"))
            {
            	CrashAndSpawn crashData = gson.fromJson(gson.toJson(msgFromServer.data), CrashAndSpawn.class);
            	
            	if(crashData != null)
            	{
            		if(crashData.name.equals(botName))
            		{
            			//Our bot has crash
            			System.out.println(botName + " has crash at tick "+gameTick);
            			writeToFile(botName + " has crash at tick "+gameTick);
            			botHasCrash = true;
            			botHasSpawn = false;
            			botHasCrashCurLap = true;
            		}
            	}
            }
            else if(msgFromServer.msgType.equals("spawn"))
            {
            	CrashAndSpawn crashData = gson.fromJson(gson.toJson(msgFromServer.data), CrashAndSpawn.class);
            	
            	if(crashData != null)
            	{
            		if(crashData.name.equals(botName))
            		{
            			//Our bot has spawn
            			System.out.println(botName + " has spawn at tick "+gameTick);
            			writeToFile(botName + " has spawn at tick "+gameTick);
            			
            			botHasSpawn = true;
            		}
            	}
            }
            else 
            {
            	System.out.println(msgFromServer.msgType);
            	writeToFile(msgFromServer.msgType);
            	
            	if(msgFromServer.data != null)
            	{
            		System.out.println(gson.toJson(msgFromServer.data));
                	writeToFile(gson.toJson(msgFromServer.data));
            	}
            	
                send(new Ping(gameTick));
            }
        }
    }

    public String toSwitchLane(int startInd, int endInd, Track trackData)
    {

    	Piece[] bendPieces = trackData.getBendPieceFromIndex(startInd, endInd);
    	String switchDecision = "";
    	if (bendPieces.length > 1)
    	{
    		Piece curPiece;
    		int posAngle=0;
    		int negAngle=0;
    		for(int i=0; i<bendPieces.length; i++)
    		{
    			curPiece = bendPieces[i];
    			
    			//check for number of positive angle (turn right)
    			if(curPiece.angle > 0) 
    				posAngle++;
    			else
    				negAngle++;
    		}
    		
    		//if number of positive angle before the next switch > the number of negative angle, turn right
    		if(posAngle>negAngle)
    			switchDecision = "Right";
    		else if (negAngle>posAngle)
    			switchDecision = "Left";
    		else //equal - dont change lane
    			switchDecision = "";
    		
    		return switchDecision;
    	}
    	else if (bendPieces.length == 1)
    	{
    		
    		if(bendPieces[0].angle < 0)
    			switchDecision = "Left";
    		else if(bendPieces[0].angle > 0)
    			switchDecision = "Right";
    		else
    			switchDecision = "";

    		return switchDecision;
    		
    	}
    	else{ //no bend piece
    		return "";
    	}
    }
    
    
    private double getRadius(Piece curPiece, Lane[] lanes, CarPosition curCarPos)
    {
    	double radius = 0;
    	
    	//Check if piece is a bend
    	if(curPiece.radius > 0)
    	{
    		if(curPiece.angle > 0)    	
    			//Right turning bend
    			radius = curPiece.radius - (lanes[curCarPos.piecePosition.lane.endLaneIndex].distanceFromCenter);
    		else if(curPiece.angle < 0)
    			// Left turning bend
    			radius = curPiece.radius + (lanes[curCarPos.piecePosition.lane.endLaneIndex].distanceFromCenter);
    	}
    	
    	return radius;
    }
    
    private void initVariables()
    {
    	//Variables used for max speed computation
        throttlePercent = 1.0;
        curCarPieceIndex = 0;
        prevCarPieceIndex = 0;
        prevDist = 0;
        curDist = 0;
        prevPieceLength = 0;
        
        curBendPieceArrIndex = 0;
        maxCarAngle = 0;
        totalChangeInCarAngle = 0;
        
        sectionIndex = 0;
        
        speed = 0;
        prevSpeed = 0;
        logAccel = 0;
        
        botHasCrash = false;
        botHasSpawn = true;
        
        //Use back the same avg decel from qualifying race
        if(raceCount == 0)
        {
        	totalDecel = INITIAL_DECEL;
            avgDecelCount = 1;
            
            bendSectionsArr = new ArrayList<BendSection>();
            bendPiecesIndexArr = null;
            piecesArr = null;
            
            track = null;
            lanes = null;
            
            switchLanePieceIndex = null;
        }
        
        checkInitialDecel = false;
        
        //variable used for switchLane logic                
        curSwitchIndDecision = 0;        
        firstRun = true;
        lastCheckPiece = -1;                  

        //turbo test
        turboOn = false;
        turboInfo = null;
        turboTrigger = false;
        
        lapCount = 0;
    }
    
    private void send(final SendMsg msg) 
    {
        writer.println(msg.toJson());
        writer.flush();
    }
    
    private void writeToFile(String msg)
    {
    	if(WRITE_TO_FILE == true)
    	{
    		if(!msg.startsWith("Send throttle"))    			
    			pw.println(msg);
    		
    		pw.flush();
    	}
    }
}

abstract class SendMsg {
    public String toJson() {
        return new Gson().toJson(new MsgWrapper(this));
    }

    protected Object msgData() {
        return this;
    }

    protected abstract String msgType();
    
    protected int msgGameTick()
    {
    	return -1;
    };
}

class MsgWrapper 
{
    public final String msgType;
    public final Object data;
    public final int gameTick;

    MsgWrapper(final String msgType, final Object data, final int gameTick) 
    {
        this.msgType = msgType;
        this.data = data;
        this.gameTick = gameTick;
    }

    public MsgWrapper(final SendMsg sendMsg) 
    {
        this(sendMsg.msgType(), sendMsg.msgData(), sendMsg.msgGameTick());
    }
}

class Join extends SendMsg {
    public final String name;
    public final String key;

    Join(final String name, final String key) {
        this.name = name;
        this.key = key;
    }

    @Override
    protected String msgType() {
        return "join";
    }
}

class CreateRace extends SendMsg 
{
    private RaceInitData data;
    private Bot botId;
    private int gameTick; 

    CreateRace(String botName, String botKey, String trackName, String password, int carCount) 
    {
    	botId = new Bot(botName, botKey);
    	data = new RaceInitData(botId, trackName, password, carCount);
    }

    @Override
    protected String msgType() 
    {
        return "createRace";
    }
    
    @Override
    protected Object msgData() 
    {
        return data;
    }
    
    @Override
    protected int msgGameTick() 
    {
        return gameTick;
    }
    
    class RaceInitData
    {
    	private Bot botId;
    	private String trackName;
    	private String password;
    	private int carCount;
    	
    	public RaceInitData(Bot botId, String trackName, String password, int carCount)
    	{
    		this.botId = botId;
    		this.trackName = trackName;
    		this.password = password;
    		this.carCount = carCount;
    	}
    }
    
    class Bot
    {
    	private String name;
    	private String key;
    	
    	public Bot(String name, String key)
    	{
    		this.name = name;
    		this.key = key;
    	}
    }
}



class JoinRace extends SendMsg 
{
    private RaceInitData data;
    private Bot botId;
    private int gameTick; 

    JoinRace(String botName, String botKey, String trackName, String password, int carCount) 
    {
    	botId = new Bot(botName, botKey);
    	data = new RaceInitData(botId, trackName, password, carCount);
    }

    @Override
    protected String msgType() 
    {
        return "joinRace";
    }
    
    @Override
    protected Object msgData() 
    {
        return data;
    }
    
    @Override
    protected int msgGameTick() 
    {
        return gameTick;
    }
    
    class RaceInitData
    {
    	private Bot botId;
    	private String trackName;
    	private String password;
    	private int carCount;
    	
    	public RaceInitData(Bot botId, String trackName, String password, int carCount)
    	{
    		this.botId = botId;
    		this.trackName = trackName;
    		this.password = password;
    		this.carCount = carCount;
    	}
    }
    
    class Bot
    {
    	private String name;
    	private String key;
    	
    	public Bot(String name, String key)
    	{
    		this.name = name;
    		this.key = key;
    	}
    }
}

class Ping extends SendMsg {
    
	private int gameTick;
	
	public Ping(int gameTick)
	{
		this.gameTick = gameTick;
	}
	
	@Override
    protected String msgType() {
        return "ping";
    }
    
    @Override
    protected int msgGameTick() {
        return gameTick;
    }
}

class Turbo extends SendMsg 
{
	private String data;
	private int gameTick;
	
	public Turbo(String data, int gameTick){
		this.data = data;
		this.gameTick = gameTick;
	}
	
    @Override
    protected Object msgData() {
        return data;
    }

    @Override
    protected String msgType() {
        return "turbo";
    }
    
    @Override
    protected int msgGameTick() {
        return gameTick;
    }
}

class SwitchLane extends SendMsg 
{
	private String data;
	private int gameTick;
	
	public SwitchLane(String data, int gameTick){
		this.data = data;
		this.gameTick = gameTick;
	}
	
    @Override
    protected Object msgData() {
        return data;
    }

    @Override
    protected String msgType() {
        return "switchLane";
    }
    
    @Override
    protected int msgGameTick() {
        return gameTick;
    }
}

class Throttle extends SendMsg {
    private double value;
    private int gameTick;

    public Throttle(double value, int gameTick) {
        this.value = value;
        this.gameTick = gameTick;
    }

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "throttle";
    }
    
    @Override
    protected int msgGameTick() {
        return gameTick;
    }
}
