package noobbot;

import java.util.ArrayList;
import java.util.List;

/** The startingPoint attribute is a visualization cue and can be ignored. */
class GameInit
{
	public final Race race;
	
	GameInit(final Race race)
	{
		this.race = race;
	}
	
	@Override
	public String toString() {
		return "Race: " + race;
	}
}

class Race
{
	public final Track track;
	
	Race(final Track track, final Lane[] lanes)
	{
		this.track = track;
	}
	
	@Override
	public String toString()
	{
		return "track: " + track; 
	}
}

class Track 
{
	public final String id;
	public final String name;
	public final Piece[] pieces;
	public final Piece[] bendPieces = null;
	public final Lane[] lanes;
	
	Track(final String id, final String name, final Piece[] pieces, final Lane[] lanes)
	{
		this.id = id;
		this.name = name;
		this.pieces = pieces;
		this.lanes = lanes;
	}
	
	public int getInnerRingIndex()
	{
		double tempDist= 0 ;
		int index = 0;
		for(int i=0; i<lanes.length; i++)
		{
			if(lanes[i].distanceFromCenter > tempDist)
			{
				index = lanes[i].index;
				tempDist = lanes[i].distanceFromCenter;
			}
				
		}
		return index;
	}
	public int[] getSwitchLaneIndex()
	{
		List<Integer> list = new ArrayList<Integer>();
		for(int i=0; i<pieces.length; i++)
		{
			if(pieces[i].hasSwitch())
			{
				list.add(i);
			}
		}
		
		int[] intArr = new int[list.size()];
		for(int i=0; i<list.size(); i++)
		{
			intArr[i] = list.get(i);
		}
		
		return intArr;
	}
	
	//Used for independent bend piece max speed computation
	public ArrayList<Integer> getBendIndex()
	{
		ArrayList<Integer> resultArr = new ArrayList<Integer>();
		
		for(int i=0; i<pieces.length; i++)
		{
			if(pieces[i].radius > 0)
				resultArr.add(i);
		}
		
		return resultArr;
	}
	
	public Piece[] getBendPieceFromIndex(int startInd, int endInd)
	{
		List<Piece> pieceList = new ArrayList<Piece>();
		Piece curPiece;
		
		for(int i=startInd; i<endInd; i++)
		{
			curPiece = pieces[i];
				if (curPiece.isBend())
					pieceList.add(pieces[i]);			
		}
		
		Piece[] piecesArr = new Piece[pieceList.size()];
		for(int i=0; i<pieceList.size(); i++)
		{
			piecesArr[i] = pieceList.get(i);
		}
		
		return piecesArr;
	}
	
	@Override
	public String toString()
	{
		String piecesInfo = "";
		String lanesInfo = "";
		
		for(int i=0; i<pieces.length; i++)		
			piecesInfo += pieces[i].toString()+"\n";
		
		for(int i=0; i<lanes.length; i++)		
			lanesInfo += lanes[i].toString()+"\n";		
		
		return "id: " +  id + ", name: " + name + ", pieces:\n" + piecesInfo + "lanes:\n" + lanesInfo;
	}
}

/**
 * For a Straight the length attribute depicts the length of the piece. Additionally, 
 * there may be a switch attribute that indicates that this piece is a switch piece, 
 * i.e. you can switch lanes here. The bridge attribute indicates that this piece is on a bridge. 
 * This attribute is merely a visualization cue and does not affect the actual game.
 * 
 * For a Bend, there's radius that depicts the radius of the circle that would be 
 * formed by this kind of pieces. More specifically, this is the radius at the center 
 * line of the track. The angle attribute is the angle of the circular segment that 
 * the piece forms. For left turns, this is a negative value. For a 45-degree bend to
 * the left, the angle would be -45. For a 30-degree turn to the right, this would be 30.
 */

class Piece 
{
	//straight line
	public final double length;
	public final boolean bridge;
	
	//bend
	public final int radius;
	public final double angle;
	
	public final boolean switchLane;
	
	//Max speed computation
	public double latAccel;
	public double maxSpeed;
	public boolean crashOnStraightRoad;
	public int crashCount;
	
	
	Piece(final double length,final boolean switchLane,final boolean bridge,
			final int radius,final double angle)
	{
		this.length = length;
		this.switchLane = switchLane;
		this.bridge = bridge;
		this.radius = radius;
		this.angle = angle;	
		
		crashOnStraightRoad = false;
		crashCount = 0;
	}
	
	public void computeMaxSpeed(double newLatAccel)
	{
		latAccel = newLatAccel;
		
		if(radius > 0)		
			maxSpeed = Math.sqrt(latAccel*radius);
		else
			maxSpeed = -1;
	}
	
	public double getMaxSpeed()
	{
		return maxSpeed;
	}
	
	public void setLatAccel(double newLatAccel)
	{
		latAccel = newLatAccel;
		maxSpeed = Math.sqrt(latAccel*radius);
	}
	
	public double getLatAccel()
	{
		return latAccel;
	}
	
	public String pieceType()
	{
		if(radius > 0)
			return "bend";
		
		return "straight";
	}
	
	public boolean hasSwitch()
	{
		return switchLane;
	}
	
	public boolean isBend()
	{
		if(radius > 0)
			return true;
		return false;
	}
	
	@Override
	public String toString()
	{
		if(pieceType().equals("bend"))
		{
			return "PieceType: bend, switchLane: " +  switchLane + 
					", Radius: " + radius + " , Angle: " + angle; 
		}

		return "PieceType: Straight, switchLane: " + switchLane +
				", length: " +  length;
			
	}
}


/**
 * The track may have 1 to 4 lanes. The lanes are described in the protocol as an array of 
 * objects that indicate the lane's distance from the track center line. A positive value
 * tells that the lanes is to the right from the center line while a negative value indicates 
 * a lane to the left from the center.	
 * 
 */
class Lane
{
	public final double distanceFromCenter;
	public final int index;	
	
	public final int startLaneIndex;
	public final int endLaneIndex;
	
	Lane(final double distanceFromCenter, final int index, final int startLaneIndex, final int endLaneIndex)
	{
		this.distanceFromCenter = distanceFromCenter;
		this.index = index;
		
		this.startLaneIndex = startLaneIndex;
		this.endLaneIndex = endLaneIndex;
	}
	
	@Override
	public String toString()
	{
		return "distanceFromCenter: " + distanceFromCenter + ", index:" + index; 
	}
}

class CrashAndSpawn
{
	public final String name;
	
	CrashAndSpawn(final String name)
	{
		this.name = name;
	}
}

class BendSection
{
	private int startIndexOfSection;
	private int endIndexOfSection;
	
	BendSection(int startIndexOfSection, int endIndexOfSection)
	{
		this.startIndexOfSection = startIndexOfSection;
		this.endIndexOfSection = endIndexOfSection;
	}
	
	public int getStartIndexOfSection()
	{
		return this.startIndexOfSection;
	}
	
	public int getEndIndexOfSection()
	{
		return this.endIndexOfSection;
	}
	
	public String toString()
	{
		return "startIndex: "+this.startIndexOfSection+" endIndex: "+this.endIndexOfSection;
	}
}

class LapFinished
{
	public CarId car;
	
	LapFinished(CarId car)
	{
		this.car = car;
	}
}
